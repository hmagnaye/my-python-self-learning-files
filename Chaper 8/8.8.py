def make_album(artist, title, track=""):
    if track != "":
        return {artist: {title : track}}
    else:
        return {artist: title}

while True:
    artist = input("Artist")
    title = input("Title")
    trackNum = str(input("Track"))

    if artist != "" and title != "":
        if trackNum != "":
            album = make_album(artist, title, track=trackNum)
            print(album)
        else:
            album = make_album(artist, title)
            print(album)
        break
    else:
        continue

