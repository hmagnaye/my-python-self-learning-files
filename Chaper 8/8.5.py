def describe_city(city, country="AUSTRALIA"):
    print(city + " is in " + country)


describe_city(city="Sydney")
describe_city(city="Melbourne")

describe_city(city="San Francisco", country="USA")

