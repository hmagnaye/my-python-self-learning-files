def build_profile(first, last, **user_info):

    profile = {}
    profile["First Name"] = first
    profile["Last Name"] = last

    for key, value in user_info.items():
        profile[key] = value
    return profile

profile_one = build_profile("HANS", "MAGNAYE", country="AUSTRALIA", date_of_birth="22/06/99", Ethihcity="Filipino")

print(profile_one)
