def make_album(artist, title, track=""):
    if track != "":
        return {artist : {title : track}}
    else:
        return {artist: title}


one = make_album("The Stone Roses", "The Stone Roses")
print(one)


two = make_album("The Stone Roses", "The Stone Roses", track="10")
print(two)


