def make_car(maker, model, **other_details):

    car_details = {}
    car_details["Maker"] = maker
    car_details["Model"] = model

    for key, value in other_details.items():
        car_details[key] = value

    return car_details


carOne = make_car("Honda", "Civic", year="1979", color="blue")
print(carOne)