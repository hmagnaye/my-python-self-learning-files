from django.db import models

# Create your models here.
class Area_Of_Life(models.Model):
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text



class Post(models.Model):
    area_of_life = models.ForeignKey(Area_Of_Life,  on_delete=models.PROTECT)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'entries'

    def __str__(self):
        self.text_len = len(self.text)
        if self.text_len > 50:
            return self.text[:50] + "..."
        else:
            return self.text[:50]

