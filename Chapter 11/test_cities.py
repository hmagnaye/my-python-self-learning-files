import unittest

from city_functions import CityCountry
from city_function_modifed import city_country_two

class TestCity(unittest.TestCase):

    def test_city_country(self):
        city_and_country = CityCountry("Manila", "Philippines")
        self.assertEqual(city_and_country, "Manila, Philippines")

    def test_city_country_two(self):
        test_value = city_country_two("Manila", "Philippines", "24000000")
        self.assertEqual(test_value, "Manila, Philippines, population; 24000000")

    def test_city_country_two_without_population(self):
            test_value = city_country_two("Manila", "Philippines")
            self.assertEqual(test_value, "Manila, Philippines")


