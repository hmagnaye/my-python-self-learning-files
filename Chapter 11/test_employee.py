from employee import Employee

import unittest

class Test_Employee(unittest.TestCase):

    def setUp(self):
        self.test_employee = Employee("George", "Pell", 1000)

    def test_default_employee_raise(self):
        self.test_employee.give_raise()
        self.assertEqual(self.test_employee.annual_salary, 6000)

    def test_custom_employee_raise(self):
        self.test_employee.give_raise(2000)
        self.assertEqual(self.test_employee.annual_salary, 3000)

unittest.main()

