file_name = "guest.txt"



with open(file_name, "w") as name:
    stop_value = True
    while stop_value:
        name_input = input("What is your name?")

        if name_input == "stop":
            stop_value = False
            name.close()
            break
        else:
            print("Hello " + name_input)
            name.write(name_input + "\n")


