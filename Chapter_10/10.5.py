file_name = "reasons.txt"

with open(file_name, "r+") as name:
    stop_value = True

    for line in name:
        print(line)

    while stop_value:
        reason_input = input("What is your reason to program?")

        if reason_input == "stop":
            stop_value = False
            name.close()
            break
        else:
            print("Reason; " + reason_input)
            name.write(reason_input + "\n")

