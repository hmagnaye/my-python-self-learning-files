file_names = ["cats.txt", "dogs.txt"]

for file_name in file_names:
    try:
        with open(file_name, "r") as file_object:
            lines = file_object.readlines()
            for name in lines:
                print(name.rstrip())
        file_object.close()
        print("\n")
    except FileNotFoundError:
        print("Sorry there is not file by the name of; " + file_name)

