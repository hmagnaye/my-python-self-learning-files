file_name = "python.txt"

with open(file_name) as file_object:
    lines = file_object.readlines()

text_display = ""

for line in lines:
    text_display += line

print(text_display.replace("Python", "Java"))