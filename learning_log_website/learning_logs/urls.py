"""Defines URL patterns for learning_logs."""

from . import views
from django.urls import path


urlpatterns = [
    #Home page
    path('^$', views.index, name='index'),
]
