import sys

import pygame

from settings import Settings


def run_game():
# Initialize game and create a screen object.
    pygame.init()
    screen = pygame.display.set_mode((1200, 800))
    pygame.display.set_caption("Alien Invasion")
    setting_for_game = Settings()
    screen = pygame.display.set_mode((setting_for_game.screen_width, setting_for_game.screen_height))

# Start the main loop for the game.
    while True:
        screen.fill(setting_for_game.background_colour)
# Watch for keyboard and mouse events.
        for event in pygame.event.get():
           if event.type == pygame.QUIT:
               sys.exit()
      # Make the most recently drawn screen visible.
        pygame.display.flip()

run_game()