age = 100

if age < 2:
    print("baby")
elif age < 4:
    print("todder")
elif age < 13:
    print("kid")
elif age < 20:
    print("teen")
elif age < 65:
    print("adult")
elif age > 65:
    print("senior")
