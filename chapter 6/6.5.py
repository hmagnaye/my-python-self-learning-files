rivers = {"Philippines":"Pagsig", "Australia":"Murrray", "England":"Thames"}

for country, river in rivers.items():
    print("In " + country + " the River " + river + " passes by")

for country in rivers.keys():
    print("In " + country )

for river in rivers.values():
    print("In " + river)