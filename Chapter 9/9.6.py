class Restaurant():

    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine
        self.number_served = 0

    def describe_restaurant(self):
        print("The name of the restaurant is " + self.name + " and the type of cuisoine they serve is " + self.cuisine)
        print("People served; " + str(self.number_served))


    def open_restaurant(self):
        print(self.name + " is open for business")

    def increment_number_served(self, number_served_today):
        self.number_served += number_served_today


class IceCreamStand(Restaurant):

    def __init__(self, name, cuisine):
        super().__init__(name, cuisine)
        self.flavor_list = ["Choc Chip", "Mint Chip", "Mango", "Cookies and Cream"]

    def print_flavors(self):

        for flavor in self.flavor_list:
            print(flavor)



stand_one = IceCreamStand("Ming and Co", "Desert")

stand_one.print_flavors()