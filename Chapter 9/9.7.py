class User_Profile():

    def __init__(self, first_name, last_name, age, date_of_birth, location):

        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.date_of_birth = date_of_birth
        self.location = location

    def describe_user(self):
        print("Name; " + self.first_name + " " + self.last_name)
        print("Age: " + self.age)
        print("DOB; " + self.date_of_birth)
        print("Location; " + self.location)

    def greet_user(self):
        print("Hello, " + self.first_name + " " + self.last_name)


class Admin(User_Profile):

    def __init__(self, first_name, last_name, age, date_of_birth, location):
        super().__init__(first_name, last_name, age, date_of_birth, location)
        self.privileges = ["can add post", "can delete post", "can ban user"]

    def show_privileges(self):

        for privilege in self.privileges:
            print(privilege)


first_admin = Admin("William", "Gates", "65", "1955", "Seattle")

first_admin.show_privileges()
