from user_one import User_Profile


class Admin(User_Profile):

    def __init__(self, first_name, last_name, age, date_of_birth, location):
        super().__init__(first_name, last_name, age, date_of_birth, location)
        self.privileges = Privileges()




class Privileges():

    def __init__(self):
        self.privileges = ["can add post", "can delete post", "can ban user"]

    def show_privileges(self):
        for privilege in self.privileges:
            print(privilege)

