from random import randint


class Dice():

    def __init__(self, side=6):
        self.sides = side

    def roll_dice(self):
        for value in range(0,10):
            print(str(randint(1,self.sides)) + " from a " + str(self.sides) + " dice.")



dice_one = Dice()

dice_one.roll_dice()

dice_one = Dice()

dice_one.roll_dice()

dice_two = Dice(10)

dice_two.roll_dice()

dice_3 = Dice(20)

dice_3.roll_dice()


