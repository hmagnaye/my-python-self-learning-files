from rest import Restaurant

new_rest = Restaurant("KFC", "American")

rest_one = Restaurant("KFC", "American")

print(rest_one.name)
print(rest_one.cuisine)


rest_one.describe_restaurant()
rest_one.open_restaurant()

rest_one.number_served = 100
rest_one.describe_restaurant()

rest_one.increment_number_served(200)

rest_one.describe_restaurant()